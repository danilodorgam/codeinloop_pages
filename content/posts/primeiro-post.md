+++
title = 'Primeiro Post'
date = 2024-04-07T14:15:14-03:00
draft = false
+++
# Objetivo do Blog

Este é um pequeno artigo que visa mostrar o objetivo do blog.

Inicialmente nossa ideia é compartilhar conteúdo e dicas relacionados a programação e ao mundo DevOps.

Esse será um exemplo de como vamos compartilhar trechos de código:

```java
class OlaMundo{
    public static void main(String[] arguments) { 
        System.out.println("Olá Mundo!"); 
    }
}
```
Esta é uma página criada usando o Hugo Framework e publicada usando o Gitlab Pages. Para saber como fazer isso acesse nosso [artigo](https://codeinloop.com.br/criando-um-blog-com-hugo-e-gitlab-pages-simples-e-rapido/)